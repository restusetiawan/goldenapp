<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use mdm\admin\components\MenuHelper;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<style>
    .navbar-default {
        box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.2), 0 5px 10px 0 rgba(0, 0, 0, 0.19);
    }
</style>
<body style="background-color: #EEEEEE;">
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Application',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $callback = function($menu){
        $data = $menu['data'];
        //if have syntax error, unexpected 'fa' (T_STRING)  Errorexception,can use
        //$data = $menu['data'];
        return [
            'label' => $menu['name'],
            'url' => [$menu['route']],
            'option' => $data,
            'icon' => $menu['data'],
            'items' => $menu['children'],
        ];
    };

    $items = MenuHelper::getAssignedMenu(Yii::$app->user->id, null, $callback, true);
    //untuk tambahan menu logout, khusus.
    $items[] = [
        'label'=>'Logout',
        'url'=>['/site/logout'],
        'linkOptions' => ['data' => ['method' => 'post']] , 'visible' => !Yii::$app->user->isGuest
    ];

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $items,
    ]);

    NavBar::end();

    ?>

    <!--<div class="container" style="background-color: #fff; margin-top: 60px; padding: 30px; border-radius: 15px;">-->
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
