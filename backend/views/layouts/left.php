<?php use mdm\admin\components\MenuHelper; ?>
<aside class="main-sidebar">
    <section class="sidebar">
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <?php 
        $callback = function($menu){
            $data = $menu['data'];
            //if have syntax error, unexpected 'fa' (T_STRING)  Errorexception,can use
            //$data = $menu['data'];
            return [
                'label' => $menu['name'],
                'url' => [$menu['route']],
                'option' => $data,
                'icon' => $menu['data'],
                'items' => $menu['children'],
            ];
        };
        $items = MenuHelper::getAssignedMenu(Yii::$app->user->id, null, $callback, true);
        //untuk tambahan menu logout, khusus.
        //$items[] = [
          //  'label'=>'Logout',
            //'url'=>['/site/logout'],
            //'linkOptions' => ['data' => ['method' => 'post']] , 'visible' => !Yii::$app->user->isGuest
        //];
        echo dmstr\widgets\Menu::widget([
            'options' => ['class' => 'sidebar-menu'],
            'items' => $items,
        ]);
        ?>
    </section>
</aside>