<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterRow */

$this->title = Yii::t('app', 'Create Master Row');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Rows'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-row-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
