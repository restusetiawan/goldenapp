<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterDo */

$this->title = Yii::t('app', 'Create Master Do');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Dos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-do-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
