<?php

use yii\helpers\Html;
use yii\grid\GridView;

?>

<?= Html::beginForm(['site/confirm'], 'post'); ?>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
	    [
	        'class' => 'yii\grid\CheckboxColumn',
	        'checkboxOptions' => function($model, $key, $index, $widget) {
	            return ['value' => $dataProvider['id'] ];
	         },
	    ],
	        'produk',
	        'description',
	    ],
	]); ?>
<?= Html::endForm(); ?>