<?php 

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

$this->title = Yii::t('app', 'Outbound Finish Good');
$this->params['breadcrumbs'][] = $this->title;

?>
<script type="text/javascript">
	function toggle(pilih) {
	  	checkboxes = document.getElementsByName('checkbox[]');
	  	for (var i = 0, n=checkboxes.length;i<n;i++) {
	    	checkboxes[i].checked = pilih.checked;
	  	}
	}
	function toggleSugar(pilih) {
	  	checkboxes = document.getElementsByName('checkboxSugar[]');
	  	for (var i = 0, n=checkboxes.length;i<n;i++) {
	    	checkboxes[i].checked = pilih.checked;
	  	}
	}
</script>
<?php $form = ActiveForm::begin(); ?>
	<div class="form-group">
		<input type="text" class="form-control" name="no_sto" placeholder="Masukan Nomor STO" required>
	</div>
	<div class="form-group">
	<?= Html::submitButton('<i class="fa fa-spinner"></i>  Confirm', [ 'id' => 'update', 'class' => 'btn btn-primary btn-sm']) ?>
	<?= Html::submitButton('<i class="fa fa-refresh"></i>  Refresh', [ 'id' => 'update', 'class' => 'btn btn-default btn-sm']) ?>
	</div>
    <table class="table table-bordered table-hover table-striped">
	<thead>
		<th><input type="checkbox" name="checkboxall" onClick="toggle(this)"></th>
		<th>No</th>
		<th>Produk</th>
		<!--<th>Decription</th>-->
		<th>Batch</th>
		<th>Hu Number</th>
		<th>Quantity</th>
		<th>No Truck</th>
		<th>Nama Supir</th>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($modelsFg as $model) {  ?>
			<tr id="rows[]">
				<td>
					<input type="checkbox" name="checkbox[]" id="checkbox" value="<?php echo $model['id']; ?>" />
				</td>
				<td><?php echo $no; ?></td>
				<td>
					<?php echo $model->produk; ?>
					<?php echo $form->field($model, 'produk[]')->hiddenInput([
						'value' => $model['produk'],
						'readOnly' => true
					])->label(false) ?>
				</td>
				<!--<td>
					<?php //echo $form->field($model, 'description[]')->textInput([
						//'value' => $model['description'],
						//'readOnly' => true
					//])->label(false) ?>
				</td>-->
				<td>
					<?php echo $model->batch; ?>
					<?php echo $form->field($model, 'batch[]')->hiddenInput([
						'value' => $model['batch'],
						'readOnly' => true
					])->label(false) ?>
				</td>
				<td>
					<?php echo $model->hu_number; ?>
					<?php echo $form->field($model, 'hu_number[]')->hiddenInput([
						'value' => $model['hu_number'],
						'readOnly' => true
					])->label(false) ?>
				</td>
				<td>
					<?php echo $model->quantity; ?>
					<?php echo $form->field($model, 'quantity[]')->hiddenInput([
						'value' => $model['quantity'],
						'readOnly' => true
					])->label(false) ?>
				</td>
				<td>
					<?php echo $model->no_pol; ?>
					<?php echo $form->field($model, 'no_pol[]')->hiddenInput([
						'value' => $model['no_pol'],
						'readOnly' => true
					])->label(false) ?>
				</td>
				<td>
					<?php echo $model->driver_name; ?>
					<?php echo $form->field($model, 'driver_name[]')->hiddenInput([
						'value' => $model['driver_name'],
						'readOnly' => true
					])->label(false) ?>
				</td>
			</tr>
		<?php $no++; } ?>
	</tbody>
</table>
<?php ActiveForm::end() ?>
<?php 
	echo \yii\widgets\LinkPager::widget([
		'pagination' => $pagesFg,
	]);
?>