<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterData */

$this->title = Yii::t('app', ' Scanning confirmation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-data-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->renderAjax('_form', [
        'model' => $model,
    ]) ?>

</div>
