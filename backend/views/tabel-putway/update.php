<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TabelPutway */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Tabel Putway',
]) . $model->putway_id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tabel Putways'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->putway_id, 'url' => ['view', 'id' => $model->putway_id]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tabel-putway-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
