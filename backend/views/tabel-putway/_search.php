<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TabelPutwaySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tabel-putway-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'putway_id') ?>

    <?= $form->field($model, 'hu_number') ?>

    <?= $form->field($model, 'to_lokasi') ?>

    <?= $form->field($model, 'to_row') ?>

    <?= $form->field($model, 'to_level') ?>

    <?php // echo $form->field($model, 'refrence') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
