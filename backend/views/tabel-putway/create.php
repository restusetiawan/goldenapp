<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TabelPutway */

$this->title = Yii::t('app', 'Putway Proses');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tabel Putways'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tabel-putway-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
