<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\builder\TabularForm;
use yii\widgets\ActiveForm;
//use kartik\grid\GridView;
//use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Result Outbound');
$this->params['breadcrumbs'][] = $this->title;
?>
<script type="text/javascript">
      $(document).on('click', '.showModalButton', function(){
        if ($('#modal').data('bs.modal').isShown) {
            $('#modal').find('#modalContent')
                    .load($(this).attr('value'));
            document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
        } else {
            $('#modal').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('value'));
            document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
        }
    });
</script>
<div class="master-data-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
    <?php echo Html::button(Yii::t('app', 'Upload Data Outbound'), ['value' => Url::to(['site/upload']), 
        'class' =>  ' showModalButton btn btn-success', 'id' => 'modalButton'
    ]) ?>
    </p>
<?php 
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header'=> '<h3>Upload Data</h3>',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>    
<?php Pjax::begin(); ?>

<?php 
/*$form = ActiveForm::begin(); 
echo TabularForm::widget([
    'form' => $form,
    'dataProvider' => $dataProvider,
    'attributes' => [
        'produk' => ['type' => TabularForm::INPUT_HIDDEN_STATIC],
        'description' => ['type' => TabularForm::INPUT_HIDDEN_STATIC],
        'batch' => ['type' => TabularForm::INPUT_HIDDEN_STATIC],
        'hu_number' => ['type' => TabularForm::INPUT_HIDDEN_STATIC],
        'quantity' => ['type' => TabularForm::INPUT_HIDDEN_STATIC],
    ],
    'gridSettings' => [
        'floatHeader' => true,
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  Master Data</h3>',
            //'type' => GridView::TYPE_PRIMARY,
            'after' => Html::submitButton(
                    '<i class="glyphicon glyphicon-floppy-disk"></i> Save', 
                    ['class'=>'btn btn-primary']
                )
        ],
    ],
]);
ActiveForm::end();
*/?>

<?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],

            //'id',
            //'activity',
            /*[
                'attribute' => 'activity',
                'filter' => \kartik\widgets\Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'activity',
                        'data' => [
                            'Inbound' => 'Inbound',
                            'Outbound' => 'Outbound'
                        ],
                        'options' => ['placeholder' => 'Select'],
                        'pluginOptions' => ['allowClear' => true]
                    ]),
                
            ],*/
            //'type_produk',
            [
                'attribute' => 'type_produk',
                'filter' => \kartik\widgets\Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'type_produk',
                        'data' => \yii\helpers\ArrayHelper::map(\common\models\MasterType::find()->all(),'produk_type', 'produk_type'),
                        'options' => ['placeholder' => 'Select'],
                        'pluginOptions' => ['allowClear' => true]
                    ]),
                
            ],
            'produk',
            'description',
            'batch',
            'hu_number:ntext',
            'quantity',
            'no_sto',
            'no_pol',
            'driver_name',
            // 'hu_number_key',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => \kartik\widgets\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => [
                        'new' => 'new',
                        'scan' => 'scan',
                        'confirm' => 'confirm',
                        'done' => 'done'
                    ],
                    'options' => ['placeholder' => 'Select'],
                    'pluginOptions' => ['allowClear' => true]
                ]),
                'value' => function ($model) {
                    if ($model->status === 'scan') {
                        return "<span class='label label-warning'>Scann</span>";
                    } elseif ($model->status === 'new') {
                        return "<span class='label label-success'>New</span>";
                    } elseif($model->status === 'confirm') {
                        return "<span class='label label-primary'>Confirm</span>";
                    } else {
                        return "<span class='label label-default'>done</span>";
                    }
                },
            ],
            //'status',
            //'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

<?php Pjax::end(); ?></div>
