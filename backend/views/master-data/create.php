<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterData */

$this->title = Yii::t('app', 'Create Master Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
