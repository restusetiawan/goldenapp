<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Dc */

$this->title = Yii::t('app', 'Create Dc');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dcs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
