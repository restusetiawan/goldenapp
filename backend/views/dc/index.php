<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel common\models\DcSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Data Delivery Center');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dc-index">
    <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Yii::t('app', 'Create Dc'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php 
    $form = ActiveForm::begin([
                'action' => '/dc/create', 
                'method'=> 'post',
            ]); ?>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions' => function ($model) {
                if ($model->dc_id == 2) {
                    return ['class' => 'danger'];
                } else {
                    return ['class' => 'success'];
                }
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'name' => 'Dc[dc_id]',
                    'checkboxOptions' => function ($model, $key, $index, $column) {
                        return ['value' => $model->dc_id];
                    },
                    'multiple' => true,
                ],
                'dc_id',
                'name',
                'code',
                'city',
                'province',
                // 'group',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>   
    <?php ActiveForm::end(); ?>                
</div>