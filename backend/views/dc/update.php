<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Dc */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Dc',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dcs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->dc_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
