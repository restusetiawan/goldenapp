<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterLokasi */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Master Lokasi',
]) . $model->lokasi_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Lokasis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lokasi_id, 'url' => ['view', 'id' => $model->lokasi_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="master-lokasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
