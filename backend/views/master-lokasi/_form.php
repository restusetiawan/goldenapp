<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MasterLokasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-lokasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'lokasi_name')->textInput(['maxlength' => true]) ?>

	<?php $data = ['BULKY'=>Yii::t('app', 'BULKY'),'RACKING'=>Yii::t('app', 'RACKING')]; ?>

    <?= $form->field($model, 'lokasi_type')->dropdownList($data, ['prompt' => 'Pilih Type lokasi']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
