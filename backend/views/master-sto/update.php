<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterSto */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Master Sto',
]) . $model->sto_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Stos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sto_id, 'url' => ['view', 'id' => $model->sto_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="master-sto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
