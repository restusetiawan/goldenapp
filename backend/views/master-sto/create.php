<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterSto */

$this->title = Yii::t('app', 'Create Master Sto');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Stos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-sto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
