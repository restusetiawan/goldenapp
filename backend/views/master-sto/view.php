<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterSto */

$this->title = $model->sto_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Stos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-sto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->sto_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->sto_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sto_id',
            'no_sto',
            'create_at',
            'update_at',
        ],
    ]) ?>

</div>
