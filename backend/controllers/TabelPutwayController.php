<?php

namespace backend\controllers;

use Yii;
use common\models\TabelPutway;
use common\models\TabelPutwaySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TabelPutwayController implements the CRUD actions for TabelPutway model.
 */
class TabelPutwayController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TabelPutway models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TabelPutwaySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TabelPutway model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TabelPutway model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TabelPutway();

        if ($model->load(Yii::$app->request->post())) {
            $status = 'scan';
            $model->status = $status;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->putway_id]);
            }
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TabelPutway model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $status = 'scan';
            $model->status = $status;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->putway_id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TabelPutway model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TabelPutway model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TabelPutway the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TabelPutway::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPutway_confirmation()
    {
        $data = TabelPutway::find()->where(['status' => 'scan']);
        $count = clone $data;
        $pages = new \yii\data\Pagination(['totalCount' => $count->count()]);
        $models = $data->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        if (isset($_POST['checkbox'])) {
            $status = 'done';
            $refrence = Yii::$app->request->post('refrence');
            foreach ($_POST['checkbox'] as $key => $checkbox) {
                $check = ($checkbox);
                $update = "UPDATE tabel_putway SET status = '$status', refrence = '$refrence' WHERE putway_id = '$check' ";

                $query = \Yii::$app->db->createCommand($update)->execute();
            }
            if ($query) {
                $modelDo = new \common\models\MasterDo();
                $modelDo->no_do = $refrence;
                $modelDo->save();
            }
            $this->redirect(['/site/index']);
        }
        return $this->render('putway', [
                'models' => $models,
                'pages' => $pages,
            ]);
    }

    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $status = 'scan';
            $model->status = $status;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Update Data Sukses');
                return $this->redirect(['putway_confirmation']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
}
