<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\UploadForm;
use common\models\MasterData;
use common\models\MasterDataSearch;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use common\models\SearchForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'logout', 
                            'index', 
                            'upload', 
                            'tree', 
                            'data', 
                            'confirm', 
                            'search', 
                            'send',
                            'outbound',
                            'outbound_sugar',
                            'export',
                            'putway',
                            'export_do'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Function Untuk action confirmation STO
     *
     * @return string
     */

    protected function findModel($id)
    {
        if (($model = MasterData::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionExport()
    {
        $model = new \common\models\MasterSto();
        $request = Yii::$app->request;
        $db = Yii::$app->db;

        $no_sto = $request->post('no_sto');

        if (!is_null($no_sto)) {
            $customer = "FFI";
            $customer_code = 5;
            $tanggal = date("d/m/Y");
            $separator = "\t";
            $filename = "data_".$no_sto.".txt";
            $dataSearch = new MasterDataSearch();
            $query = \common\models\MasterData::find()->where(['no_sto' => $no_sto])->all();

            foreach ($query as $data) {
                echo 
                $customer.$separator.
                $customer_code.$separator.
                $tanggal.$separator.
                $no_sto.$separator.
                $separator.$separator.
                $data['produk'].$separator.
                $data['hu_number'].$separator.
                $data['quantity']."\r\n";
            }
            header('Content-type: text/plain');
            header('Content-Disposition: attachment; filename='.$filename);

            Yii::$app->end();      
        } 
        return $this->render('export', ['model' => $model]);
    }

    public function actionExport_do()
    {
        $model = new \common\models\MasterDo();
        $request = \Yii::$app->request;
        $db = \Yii::$app->db;

        $no_do = $request->post('no_do');

        if (!is_null($no_do)) {
            $separator = "\t";
            $filename = "data_".$no_do.".txt";
            $query = \common\models\TabelPutway::find()->where(['refrence' => $no_do])->all();

            foreach ($query as $data) {
                echo 
                $data['hu_number'].$separator.
                $data['to_lokasi'].$separator.
                $data['to_row'].$separator.
                $data['to_level'].$separator.
                $data['refrence'].$separator.
                $data['create_at'].$separator."\r\n";
            }
            header('Content-type: text/plain');
            header('Content-Disposition: attachment; filename='.$filename);

            Yii::$app->end();      
        }
        return $this->render('export-do', ['model' => $model]);
    }
    
    public function actionUpload()
    {
        $model = new UploadForm();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            $filename = 'Data.'.$file->extension;
            $upload = $file->saveAs('uploads/'.$filename);

            if ($upload){
                define('CSV_PATH', 'uploads/');

                $csv_file = CSV_PATH.$filename;
                $filecsv = file($csv_file);
                print_r($filecsv);
                //die;

                foreach ($filecsv as $data) {

                    $modelnew = new MasterData();

                    $hasil = explode(",", $data);
                    $activity = $hasil[0];
                    $type = $hasil[1];
                    $produk = $hasil[2];
                    $description = $hasil[3];
                    $batch = $hasil[4];
                    $hu_number = $hasil[5];
                    $quantity = $hasil[6];
                    $no_pol =  $hasil[7];
                    $driver = $hasil[8];
                    $hu_number_key = substr($hu_number, -6);
                    $status = 'new';

                    $modelnew->activity  = $activity;
                    $modelnew->type_produk = $type;
                    $modelnew->produk = $produk;
                    $modelnew->description = $description;
                    $modelnew->batch  = $batch;
                    $modelnew->hu_number = $hu_number;
                    $modelnew->quantity = $quantity;
                    $modelnew->no_pol = $no_pol;
                    $modelnew->driver_name = $driver;
                    $modelnew->hu_number_key = $hu_number_key;
                    $modelnew->status = $status;

                    $modelnew->save(); 
                }

                unlink('uploads/'.$filename);
                Yii::$app->session->setFlash('success', '<span class="glyphicon glyphicon-info-sign"></span>  <b>Data Success di Upload</b>');
                return $this->redirect(['master-data/index']);
            } 
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('upload', [
                'model' => $model,
            ]);
        }else {
            return $this->render('upload', [
                'model' => $model,
            ]);
        } 
    }

    public function actionSearch()
    {
        $searchModel = new MasterDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new MasterData();

        return $this->render('search', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }

    public function actionSend($hu_number)
    {
        $model = MasterData::find()->where(['hu_number' => $hu_number])->one();
        $count = MasterData::find()->where(['status' => 'scan'])->count();
        $status = 'scan';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Sukses Kirim data');

            return $this->redirect(['/site/search']);
        } else {
            return $this->render('create', [
                    'model' => $model,
                    $model->status = $status,
                ]);
        }
    }

    /**
     * Function untuk menampilkan data dari table master_data
     *
     * @return string
     */
    public function actionData()
    {
        $data = MasterData::find()->where(['status' => 'scan']);
        $countQuery = clone $data;
        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $models = $data->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        
        if (isset($_POST['checkbox'])) {
            $status = 'confirm';
            $no_pol = Yii::$app->request->post('no_pol');
            $nama_supir = Yii::$app->request->post('driver_name');
            foreach ($_POST['checkbox'] as $index => $checkbox) {
                $check = ($checkbox);
                $update = "UPDATE master_data SET status='$status', no_pol='$no_pol', driver_name='$nama_supir' WHERE id='$check' ";

                Yii::$app->db->createCommand($update)->execute();
            }
            $this->redirect(['/site/data']);
        }
        return $this->render('data', [
                'models' => $models,
                'pages' => $pages,
            ]);
    }

    public function actionOutbound()
    {
        $query = MasterData::find()->where(['activity' => 'outbound', 'type_produk' => 'FG', 'status' => 'confirm']);
        $countQuery = clone $query;
        $pagesFg = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $modelsFg = $query->offset($pagesFg->offset)
            ->limit($pagesFg->limit)
            ->all();
        if (isset($_POST['checkbox'])) {
            $status = 'done';
            $no_sto = Yii::$app->request->post('no_sto');
            //$nama_supir = Yii::$app->request->post('nama_supir');
            foreach ($_POST['checkbox'] as $index => $checkbox) {
                $check = ($checkbox);
                $update = "UPDATE master_data SET status='$status', no_sto='$no_sto' WHERE id='$check' ";

                $query = Yii::$app->db->createCommand($update)->execute();
            }
            if ($query) {
                $modelSto = new \common\models\MasterSto();
                $modelSto->no_sto = $no_sto;
                $modelSto->save();
            }
            $this->redirect(['/site/outbound']);
        }
        return $this->render('outbound', [
                'modelsFg' => $modelsFg,
                'pagesFg' => $pagesFg,
            ]);
    }

    public function actionOutbound_sugar()
    {
        $query = MasterData::find()->where(['activity' => 'outbound', 'type_produk' => 'gula', 'status' => 'confirm']);
        $countQuery = clone $query;
        $pagesSugar = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $modelsSugar = $query->offset($pagesSugar->offset)
            ->limit($pagesSugar->limit)
            ->all();
        if (isset($_POST['checkbox'])) {
            $status = 'done';
            $no_sto = Yii::$app->request->post('no_sto');
            //$nama_supir = Yii::$app->request->post('nama_supir');
            foreach ($_POST['checkbox'] as $index => $checkbox) {
                $check = ($checkbox);
                $update = "UPDATE master_data SET status='$status', no_sto='$no_sto' WHERE id='$check' ";

                $query = Yii::$app->db->createCommand($update)->execute();
            }
            if ($query) {
                $modelSto = new \common\models\MasterSto();
                $modelSto->no_sto = $no_sto;
                $modelSto->save();
            }
            $this->redirect(['/site/outbound_sugar']);
        }


        return $this->render('outbound-sugar', [
                'modelsSugar' => $modelsSugar,
                'pagesSugar'  => $pagesSugar,
            ]);
    }

    public function actionPutway()
    {   
        $model = new \common\models\TabelPutway();

        if ($model->load(Yii::$app->request->post())) {
            $status = 'scan';
            $model->status = $status;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Sukses Putway');
                return $this->redirect(['/site/putway']);
            }
        } else {
            return $this->render('/tabel-putway/create', [
                'model' => $model,
            ]);
        }
    }
}
