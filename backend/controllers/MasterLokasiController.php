<?php

namespace backend\controllers;

use Yii;
use common\models\MasterLokasi;
use common\models\MasterLokasiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\UploadForm;
use yii\web\UploadedFile;

/**
 * MasterLokasiController implements the CRUD actions for MasterLokasi model.
 */
class MasterLokasiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterLokasi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterLokasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterLokasi model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterLokasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterLokasi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->lokasi_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MasterLokasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->lokasi_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterLokasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterLokasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterLokasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterLokasi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpload()
    {
        $model = new UploadForm();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            $filename = 'Data.'.$file->extension;
            $upload = $file->saveAs('uploads/'.$filename);

            if ($upload){
                define('CSV_PATH', 'uploads/');

                $csv_file = CSV_PATH.$filename;
                $filecsv = file($csv_file);
                print_r($filecsv);
                //die;

                foreach ($filecsv as $data) {

                    $modelnew = new MasterLokasi();

                    $hasil = explode(",", $data);
                    $lokasi = $hasil[0];
                    $lokasi_type = $hasil[1];
    
                    $modelnew->lokasi_name  = $lokasi;
                    $modelnew->lokasi_type = $lokasi_type;
                    $modelnew->save();
                }

                unlink('uploads/'.$filename);
                Yii::$app->session->setFlash('success', '<span class="glyphicon glyphicon-info-sign"></span>  <b>Data Success di Upload</b>');
                return $this->redirect(['master-lokasi/index']);
            } 
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('upload', [
                'model' => $model,
            ]);
        }else {
            return $this->render('upload', [
                'model' => $model,
            ]);
        } 
    }
}
