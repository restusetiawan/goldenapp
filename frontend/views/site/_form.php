<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MasterData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'produk')->textInput(['maxlength' => true, 'readOnly' => true])->label(false) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'readOnly' => true])->label(false) ?>

    <?= $form->field($model, 'batch')->textInput(['maxlength' => true,'readOnly' => true])->label(false) ?>

    <?= $form->field($model, 'quantity')->textInput()->label(false) ?>

    <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Input', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
