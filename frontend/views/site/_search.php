<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
	'action' => ['search'], 
	'method' => 'get',
]); ?>

<?= $form->field($model, 'hu_number')->textInput(['class' => 'form-control', 'placeholder' => 'Masukan HU Number'])->label(false) ?>
<div class="form-group">
	<?= Html::submitButton('Search', ['class' => 'btn btn-primary']); ?>
</div>

<?php ActiveForm::end(); ?>