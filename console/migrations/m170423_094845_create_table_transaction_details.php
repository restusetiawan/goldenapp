<?php

use yii\db\Migration;

class m170423_094845_create_table_transaction_details extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        }
        $this->createTable('{{%transaction_details}}', [
            'id' => $this->primaryKey(),
            'trans_id' => $this->integer(100)->notNull(),
            'item_id' => $this->integer(100),
            'quantity' => $this->decimal(15, 2)->notNull(),
            'remaks' => $this->string(100),
        ]);

        //foreign key
        $this->addForeignKey(
            'fk_transaction_details_transactions',
            'transaction_details', 'trans_id',
            'transactions', 'id',
            'restrict', 'cascade'
        );

        $this->addForeignKey(
            'fk_transaction_details_items',
            'transaction_details', 'item_id',
            'items', 'id',
            'restrict', 'cascade'
        );
    }

    public function down()
    {
        $this->dropTable('transaction_details');
        echo "m170423_094845_create_table_transaction_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
