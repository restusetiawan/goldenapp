<?php

use yii\db\Migration;
use yii\db\Schema;

class m170423_094831_create_table_transactions extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        }
        $this->createTable('{{%transactions}}', [
            'id' => $this->primaryKey(),
            'trans_code' => $this->string(100)->notNull(),
            'trans_date' => $this->date(),
            'type_id' => $this->integer(100)->notNull(),
            'remaks' => $this->string(100)->notNull(),
        ]);

        //Foreign keys
        $this->addForeignKey(
            'fk_transactions_types',
            'transactions', 
            'type_id',
            'transaction_types', 
            'id',
            'restrict', 
            'cascade'
        );
    }

    public function down()
    {
        $this->dropTable('transactions');
        echo "m170423_094831_create_table_transactions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
