<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterLokasi;

/**
 * MasterLokasiSearch represents the model behind the search form about `common\models\MasterLokasi`.
 */
class MasterLokasiSearch extends MasterLokasi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lokasi_id'], 'integer'],
            [['lokasi_name', 'lokasi_type', 'create_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterLokasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lokasi_id' => $this->lokasi_id,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['like', 'lokasi_name', $this->lokasi_name])
            ->andFilterWhere(['like', 'lokasi_type', $this->lokasi_type]);

        return $dataProvider;
    }
}
