<?php 

namespace common\models;

use Yii;
use yii\base\Model;


/**
* 
*/
class UploadForm extends Model
{
	
	public $file;

	public function rules()
	{
		return [
			[['file'], 'required'],
			[['file'], 'file', 'extensions' => 'csv', 'maxSize' => 5*1024*1024],
		];
	}

	public function attributeLabels()
	{
		return [
			'file' => 'Select File',
		];
	}
}

?>