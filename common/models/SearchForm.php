<?php 

namespace common\models;

use Yii;
use yii\base\Model;

/**
* 
*/
class SearchForm extends Model
{
	
	public $search;

	public function rules()
	{
		return [
			[['search'], 'required'],
			[['search'], 'integer'],
		];
	}

	public function attributeLabels()
	{
		return [
			'search' => 'Cari HU Number',
		];
	}
}

?>