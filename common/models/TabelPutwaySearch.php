<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TabelPutway;

/**
 * TabelPutwaySearch represents the model behind the search form about `common\models\TabelPutway`.
 */
class TabelPutwaySearch extends TabelPutway
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['putway_id', 'to_level'], 'integer'],
            [['type_produk', 'hu_number', 'to_lokasi', 'to_row', 'refrence', 'status', 'create_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TabelPutway::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'putway_id' => $this->putway_id,
            'to_level' => $this->to_level,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['like', 'hu_number', $this->hu_number])
            ->andFilterWhere(['like', 'type_produk', $this->type_produk])
            ->andFilterWhere(['like', 'to_lokasi', $this->to_lokasi])
            ->andFilterWhere(['like', 'to_row', $this->to_row])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'refrence', $this->refrence]);

        return $dataProvider;
    }
}
