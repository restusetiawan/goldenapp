<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "dc".
 *
 * @property integer $dc_id
 * @property string $name
 * @property string $code
 * @property string $city
 * @property string $province
 * @property string $group
 */
class Dc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['name', 'code', 'city', 'province', 'group'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dc_id' => Yii::t('app', 'Dc ID'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'city' => Yii::t('app', 'City'),
            'province' => Yii::t('app', 'Province'),
            'group' => Yii::t('app', 'Group'),
        ];
    }
}
